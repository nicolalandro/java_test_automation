import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StringFormatterTest {
    String text = "Text";
    StringFormatter s = new StringFormatter(text);

    @Test
    void constructorSetStringCorrectly(){
        assertEquals(text, s.getStr());
    }

    @Test
    void toLowerWork(){
        assertEquals("text", s.toLower());
    }
}
