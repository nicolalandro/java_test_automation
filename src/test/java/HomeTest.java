import javafx.stage.Stage;
import org.junit.jupiter.api.Test;
import org.testfx.api.FxAssert;
import org.testfx.framework.junit5.ApplicationTest;

import static org.testfx.util.NodeQueryUtils.hasText;

public class HomeTest extends ApplicationTest {
    @Override
    public void start(Stage stage) throws Exception {
        Main mMain = new Main();
        mMain.start(stage);
    }

    @Test
    void sample_test() {
        clickOn("#mTextField");
        write("Sample Text");
        clickOn("#mButton");
        FxAssert.verifyThat("#mTextField", hasText("Sample Text"));
        FxAssert.verifyThat("#mLabel", hasText("sample text"));
    }
}
