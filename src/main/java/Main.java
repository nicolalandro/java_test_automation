import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = getParentFromFxml("home.fxml");
        root.getStylesheets().add(getClass().getResource("style.css").toExternalForm());

        Scene scene = new Scene(root);

        primaryStage.setTitle("Title");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private Parent getParentFromFxml(String resourcePath) throws IOException {
        URL fxmlUrl = ClassLoader.getSystemClassLoader().getResource(resourcePath);
        System.out.println(fxmlUrl.toString());
        FXMLLoader fxmlLoader = new FXMLLoader(fxmlUrl);
        fxmlLoader.setController(new HomeController());
        return fxmlLoader.load();
    }
}
