import javafx.scene.control.Label;

public class LabelWrapper {
    private Label mLabel;

    public LabelWrapper(Label label) {
        this.mLabel = label;
    }

    public void setText(String str) {
        mLabel.setText(str);
    }
}
