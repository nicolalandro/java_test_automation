public class StringFormatter {
    private String str;

    public StringFormatter(String str) {
        this.str = str;
    }

    public String getStr() {
        return str;
    }

    public String toLower(){
        return str.toLowerCase();
    }
}
