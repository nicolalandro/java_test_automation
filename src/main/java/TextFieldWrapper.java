import javafx.scene.control.TextField;

public class TextFieldWrapper {
    private TextField mTextField;

    public TextFieldWrapper(TextField textField) {
        this.mTextField = textField;
    }

    public String getText(){
        return mTextField.getText();
    }
}
