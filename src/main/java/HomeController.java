import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class HomeController implements Initializable {
    @FXML
    Label mLabel;
    @FXML
    TextField mTextField;

    TextFieldWrapper mTextFieldWrapper;
    LabelWrapper mLabelWrapper;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        mLabelWrapper = new LabelWrapper(mLabel);
        mLabelWrapper.setText("Initializate");
        mTextFieldWrapper = new TextFieldWrapper(mTextField);
    }

    public void onButtonClick() {
        if (mTextFieldWrapper.getText() != null && ! mTextFieldWrapper.getText().equals("")) {
            String text = new StringFormatter(mTextFieldWrapper.getText()).toLower();
            mLabelWrapper.setText(text);
        }
    }
}
