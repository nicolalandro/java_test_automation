# Java Test Automation
In this repo I create a simple java fx project with gradle and I test it.

## Related tool
* [Gluon Scenebuilder](https://gluonhq.com/products/scene-builder/#download)
* [Maven repo](https://mvnrepository.com/)
* [JavaFX(openfx)](https://openjfx.io/)
* [JUnit 5 API](https://junit.org/junit5/docs/current/api/index.html)
* [JUnit 4](https://junit.org/junit4/)
* [Gradle](https://gradle.org/)
* [Intellij Idea Community](https://www.jetbrains.com/idea/download/index.html)

## Eclipse Shor-cut
(if you use Idea you can use Ctrl+Shift+A and search for keymap, and set it to Eclipse)
* Extract local variable: Alt+Shift+L
* Extract fild: Ctrl+Alt+F
* Extract method: Ctrl+Alt+M
* Rename: Alt+Shift+R
* Insert constructor or Override methods: Alt+ins
* Resolve a warning or suggestion: Alt+Enter
* Suggestion: Ctrl+Space
* "System.out.println();": write "sout" and press Enter


## Test Pyramid [(info)](https://jamescrisp.org/2011/05/30/automated-testing-and-the-test-pyramid/)
A test is a particular situation that the component over test should be manage. The test canonically are subdivided in types:
* Unit
  * test the behavior of one particular object
  * simple to write
  * fast to execute
* Integration
  * test the integration between two or more software components
  * not so difficult to write
  * fast to execute
* Acceptance
  * test the all behavior of a feature, simulate what an user do
  * difficult to write
  * slow to execute

[Other kind of classification](https://code.tutsplus.com/tutorials/deciphering-testing-jargon--net-27513)


## Test Double [(info)](https://it.wikipedia.org/wiki/Test_double)
The "double" is an object with the same interface of a real object but with a semplified behavior.
* Dummy: object with no behavior.
* Stub: fixed behavior, for example method that return always the same thing.
* Spy: the object memorize the message that recive in order to verify the behavior
* Mock: is a variant of the spy that explicitly memorize the codition to verify
* Fake: is an object that simulate all real object behaviour for example implement a db in memory instead of a real db

## Dependecy injection [(info)](https://it.wikipedia.org/wiki/Dependency_injection)
DI is a typical design pattern of object oriented programming. It improve the testability of an object.
An Object get in his constructor the object that we want use in the test (called dependecy of object) so it is easy to inject a mock dependency object.
So it contains three part:
* dependent component
* declaration of dependent component where made at the creation of the object, and is not responsability of the object to create his dependency
* injector that have the responsability to create the dependecy for the creation of the object